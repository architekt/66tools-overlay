# Copyright 2019-2022 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

# Prevent others 'init' systems from being installed by masking the packages

sys-fs/udev
sys-apps/openrc
sys-apps/systemd
sys-apps/systemd-tmpfiles
sys-apps/systemd-utils
sys-apps/sysvinit
sys-process/runit
sys-process/daemontools

# Prevent update from gentoo repository

>dev-libs/skalibs-2.13.1.1
>dev-lang/execline-2.9.3.0
>sys-apps/s6-2.11.3.2
>sys-apps/s6-rc-0.5.4.1
>sys-apps/s6-linux-utils-2.6.1.2
>sys-apps/s6-portable-utils-2.3.0.2

>net-dns/s6-dns-2.3.5.5-r1
>net-misc/s6-networking-2.5.1.3

>sys-apps/66-0.6.2.0-r2
>sys-apps/66-tools-0.0.8.0-r2
>dev-libs/oblibs-0.1.4.0-r2

>dev-libs/libgudev-238-r1
>sys-auth/elogind-252.9
>sys-fs/eudev-3.2.14
>sys-kernel/dracut-059-r7

>virtual/service-manager-1-r2
>virtual/libudev-251-r2
>virtual/udev-217-r7
>virtual/tmpfiles-0-r6
