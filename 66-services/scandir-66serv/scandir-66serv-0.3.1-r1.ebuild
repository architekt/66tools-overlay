# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Service to create a nested user scandir"
HOMEPAGE="https://framagit.org/pkg/obmods/scandir-66serv"
SRC_URI="https://framagit.org/pkg/obmods/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
IUSE="doc"

RDEPEND="
   sys-apps/66
   sys-apps/66-tools"

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
   local myconf=(
      --bindir=/bin
      --with-system-module=/usr/share/66/module
      --with-system-service=/usr/share/66/service
   )

   econf "${myconf[@]}"
}

src_compile() {
    emake DESTDIR="${D}"
}

src_install() {
    emake DESTDIR="${D}" install
    use doc && dodoc AUTHOR*
}
