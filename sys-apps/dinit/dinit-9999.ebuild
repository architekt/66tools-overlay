# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

#inherit toolchain-funcs

DESCRIPTION="service manager and init system"
HOMEPAGE="https://davmac.org/projects/dinit/"

if [[ ${PV} == *9999 ]]; then
   inherit git-r3
   EGIT_REPO_URI="https://github.com/davmac314/${PN}.git"
else
   SRC_URI="https://github.com/davmac314/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
   KEYWORDS="~amd64"
fi

LICENSE="Apache-2.0"
SLOT="0"

IUSE="dbus elogind lto sysinit test udev"
RESTRICT="!test? ( test )"

RDEPEND="
   dbus? ( sys-apps/dbus )
   elogind? ( sys-auth/elogind )
   udev? ( virtual/udev )"

src_prepare() {
   eapply_user
   sed -i -e "s|CXX=\$compiler|CXX=$(tc-getCXX)|" configs/mconfig.Linux.sh || die
   sed -i -e "s|LDFLAGS=|LDFLAGS=${LDFLAGS}|" configs/mconfig.Linux.sh || die
}

src_configure() {
#   if use !sysinit; then
#      sed -i -e 's|BUILD_SHUTDOWN=yes|BUILD_SHUTDOWN=no|' configs/mconfig.Linux.sh || die
#   fi
#   if use !lto; then
#    sed -i -e 's|test_compiler_arg "$compiler" -flto||' configs/mconfig.Linux.sh || die
#   fi

   local myconf=(
      $(use_enable sysinit shutdown)
      --enable-cgroups
   )

   econf "${myconf[@]}"
}

#src_test() {
#   emake check
#}
