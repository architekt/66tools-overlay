# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit prefix

SRC_URI="https://github.com/openrc/${PN/66-}/archive/${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

DESCRIPTION="A standalone utility to process systemd-style tmpfiles.d files"
HOMEPAGE="https://github.com/openrc/opentmpfiles"

LICENSE="BSD-2"
SLOT="0"
IUSE="selinux"

RDEPEND="
    selinux? ( sec-policy/selinux-base-policy )"

PATCHES=(
    "${FILESDIR}"/fix-variable-leakage.patch
)

S="${WORKDIR}/${PN/66-}-${PV}"

src_prepare() {
    default
    hprefixify tmpfiles.sh
}

src_install() {
    emake DESTDIR="${ED}" install

    dosym /bin/tmpfiles /bin/opentmpfiles.sh
    einstalldocs
}
