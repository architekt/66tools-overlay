# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

SRC_URI="https://framagit.org/architekt/architekt/tmpfilesd/-/archive/${PV}/${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"

DESCRIPTION="A standalone utility to process systemd-style tmpfiles.d files"
HOMEPAGE="https://github.com/juur/tmpfilesd"

LICENSE="MIT"
SLOT="0"

