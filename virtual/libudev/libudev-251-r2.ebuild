# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit multilib-build

DESCRIPTION="Virtual for libudev providers"

SLOT="0/1"
KEYWORDS="~amd64 ~arm"
IUSE="eudev systemd"
REQUIRED_USE="?? ( eudev systemd )"

RDEPEND="
   !systemd? (
      eudev? ( sys-fs/eudev[${MULTILIB_USEDEP}] )
      !eudev? ( >=sys-apps/systemd-utils-251[udev,${MULTILIB_USEDEP}] )
      )
   systemd? ( >=sys-apps/systemd-251:0/2[${MULTILIB_USEDEP}] )"
