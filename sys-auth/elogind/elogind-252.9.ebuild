# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10..12} )

SRC_URI="https://github.com/${PN}/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~arm"

inherit linux-info meson pam python-any-r1 udev xdg-utils

DESCRIPTION="The systemd project's logind, extracted to a standalone package"
HOMEPAGE="https://github.com/elogind/elogind"

LICENSE="CC0-1.0 LGPL-2.1+ public-domain"
SLOT="0"
IUSE="+acl audit debug doc +pam +policykit selinux test"
RESTRICT="!test? ( test )"

BDEPEND="
   app-text/docbook-xml-dtd:4.2
   app-text/docbook-xml-dtd:4.5
   app-text/docbook-xsl-stylesheets
   dev-util/gperf
   virtual/pkgconfig
   $(python_gen_any_dep 'dev-python/jinja[${PYTHON_USEDEP}]')
   $(python_gen_any_dep 'dev-python/lxml[${PYTHON_USEDEP}]')"
DEPEND="
   audit? ( sys-process/audit )
   sys-apps/util-linux
   sys-libs/libcap
   virtual/libudev:=
   acl? ( sys-apps/acl )
   pam? ( sys-libs/pam )
   selinux? ( sys-libs/libselinux )"
RDEPEND="${DEPEND}
   !sys-apps/systemd"
PDEPEND="
   sys-apps/dbus
   policykit? ( sys-auth/polkit )"

DOCS=( README.md)

PATCHES=(
   "${FILESDIR}/${P}-nodocs.patch"
   "${FILESDIR}/${PN}-252.9-musl-lfs.patch"
)

python_check_deps() {
   python_has_version "dev-python/jinja[${PYTHON_USEDEP}]" &&
   python_has_version "dev-python/lxml[${PYTHON_USEDEP}]"
}

pkg_setup() {
   local CONFIG_CHECK="~CGROUPS ~EPOLL ~INOTIFY_USER ~SIGNALFD ~TIMERFD"
   use kernel_linux && linux-info_pkg_setup
}

src_prepare() {
   if use elibc_musl; then
      # Some of musl-specific patches break build on the
      # glibc systems (like getdents), therefore those are
      # only used when the build is done for musl.
      PATCHES+=(
         "${FILESDIR}/${P}-musl-sigfillset.patch"
         "${FILESDIR}/${P}-musl-statx.patch"
         "${FILESDIR}/${P}-musl-rlim-max.patch"
         "${FILESDIR}/${P}-musl-getdents.patch"
         "${FILESDIR}/${P}-musl-gshadow.patch"
         "${FILESDIR}/${P}-musl-strerror_r.patch"
         "${FILESDIR}/${P}-musl-more-strerror_r.patch"
      )
   fi

   default
   xdg_environment_reset
}

src_configure() {
   python_setup

   local emesonargs=(
      --buildtype $(usex debug debug release)
      --libdir="${EPREFIX}"/usr/$(get_libdir)
      -Ddocdir="${EPREFIX}/usr/share/doc/${PF}"
      -Dhtmldir="${EPREFIX}/usr/share/doc/${PF}/html"
      -Dpamlibdir=$(getpam_mod_dir)
      -Dudevrulesdir="${EPREFIX}$(get_udevdir)"/rules.d
      -Drootlibdir="${EPREFIX}"/$(get_libdir)
      -Drootlibexecdir="${EPREFIX}"/$(get_libdir)/elogind
      -Drootprefix="${EPREFIX}/"
      -Dbashcompletiondir="${EPREFIX}/usr/share/bash-completion/completions"
      -Dman=auto
      -Dsmack=true
      -Dcgroup-controller=elogind
      -Ddefault-hierarchy=unified
      -Ddefault-kill-user-processes=false
      -Dacl=$(usex acl true false)
      -Daudit=$(usex audit true false)
      -Dhtml=$(usex doc auto false)
      -Dpam=$(usex pam true false)
      -Dselinux=$(usex selinux true false)
      -Dtests=$(usex test true false)
      -Dutmp=$(usex elibc_musl false true)
      -Dmode=release
   )

   meson_src_configure
}

src_install() {
   meson_src_install

   newinitd "${FILESDIR}"/${PN}.init-r1 ${PN}

   sed -e "s|@libdir@|$(get_libdir)|" "${FILESDIR}"/${PN}.conf.in > ${PN}.conf || die
   newconfd ${PN}.conf ${PN}
}

pkg_postinst() {
   if ! use pam; then
      ewarn "${PN} will not be managing user logins/seats without USE=\"pam\"!"
      ewarn "In other words, it will be useless for most applications."
      ewarn
   fi
   if ! use policykit; then
      ewarn "loginctl will not be able to perform privileged operations without"
      ewarn "USE=\"policykit\"! That means e.g. no suspend or hibernate."
      ewarn
   fi

   for version in ${REPLACING_VERSIONS}; do
      if ver_test "${version}" -lt 252.9; then
         elog "Starting with release 252.9 the sleep configuration is now done"
         elog "in the /etc/elogind/sleep.conf. Should you use non-default sleep"
         elog "configuration remember to migrate those to new configuration file."
      fi
   done
}
