# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="s6log user"
ACCT_USER_ID=23
ACCT_USER_HOME=/var/log/66
ACCT_USER_GROUPS=( s6log )

acct-user_add_deps

